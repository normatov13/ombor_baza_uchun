<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoldingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soldings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('atchot_id');
            $table->string('s_amount');
            $table->string('s_price');
            $table->dateTime('s_date');
            $table->timestamps();

            $table->foreign('atchot_id')->references('id')->on('atchots')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soldings');
    }
}
