<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\section;
use App\Models\part;

class SectionPartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /* Ustoz Ombor Bo'limlari Bilan , Qismlarni Bitta Seederga yozdim birini Id sini Ikkinchisiga
                    Yozish Oson Bo'lishi Uchun  */
                    
                    
        $datas = [
            'A-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
                'Part-4',
                'Part-5'
            ],
            'B-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
                'Part-4',
                'Part-5',
                'Part-6',
            ],
            'C-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
                'Part-4',
            ],
            'D-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
                'Part-4',
                'Part-5',
                'Part-6',
                'Part-7',
                'Part-8',
            ],
            'G-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
            ],
            'F-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
                'Part-4',
            ],
            'E-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
                'Part-4',
                'Part-5',
                'Part-6',
            ],
            'I-section'=>[
                'Part-1',
                'Part-2',
                'Part-3',
                'Part-4',
                'Part-5',
                'Part-6',
                'Part-7',
                'Part-8',
            ], 
            ];

          /* Egamberdi Domla, Ombor Bo'limlari Bilan , Qismlarni Bitta Seederga yozdim birini Id sini Ikkinchisiga
                    Yozish Oson Bo'lishi Uchun  */
                    

            foreach($datas as $indexData=>$data){
               $c_id = section::create(['s_name'=>$indexData]);
               foreach($data as $da){
                   part::create(['p_name'=>$da,'section_id'=>$c_id->id]);
               }
            }

    }
}
