<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\category;
use App\Models\product;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

                
 /* Egamberdi Domla, Ctegory  Bilan ,Products  Bitta Seederga yozdim birini Id sini Ikkinchisiga
                    Yozish Oson Bo'lishi Uchun  */

        $datas = [
            'Sabzavotlar'=>[
                ['name'=>'Pomidor','date'=>2],
                ['name'=>'Sabzi','date'=>8],
                ['name'=>'Kartoshka','date'=>8],
                ['name'=>'Piyoz','date'=>6],
                ['name'=>'Karam','date'=>5]
            ],
           'Mevalar'=>[
                ['name'=>'Olma','date'=>6],
                ['name'=>'Anor','date'=>6],
                ['name'=>'Nok','date'=>6],
                ['name'=>'Uzum','date'=>6],
                ['name'=>'Behi','date'=>6],
                ['name'=>'Shaftoli','date'=>6],
                ['name'=>'Banan','date'=>6],
                ['name'=>'Olxori','date'=>6]
           ],
            'Salqin Ichimliklar'=>[
                ['name'=>'Aqua','date'=>6],
                ['name'=>'Fanta','date'=>6],
                ['name'=>'Coca Cola','date'=>6],
                ['name'=>'Montella','date'=>6],
                ['name'=>'Dena','date'=>6],
                ['name'=>'Jesco','date'=>6],
                ['name'=>'Dolce','date'=>6]
                
            ],
            'Spirtli Ichimliklar'=>[
                ['name'=>'Toshkent','date'=>24],
                ['name'=>'Vino','date'=>24],
                ['name'=>'Aroq','date'=>24],
                ['name'=>'Kanyak','date'=>24],    
            ],
            'Sut maxsulotlari'=>[
                ['name'=>'Nestle','date'=>3],
                ['name'=>'Dev Crem','date'=>4],
                ['name'=>'Dev Maloko','date'=>2],
                ['name'=>'Sut','date'=>2],
            ],

            "G'o'sht maxsulotlari"=>[
                ['name'=>'Quruq go\'sht','date'=>3],
                ['name'=>'Xom Go\'sht','date'=>4],
                ['name'=>'Qaynatilgan Go\'sht','date'=>2],
                ['name'=>'Laxm Go\'sht','date'=>2],
            ],
            'Konserva maxsulotlari'=>[
                ['name'=>'Konserva Baliq','date'=>3],
                ['name'=>'Konserva Murabo','date'=>4],
                ['name'=>'Qora Ikra','date'=>2],
                ['name'=>'Qizil Ikra','date'=>2],
            ],
            'Poliz ekinlari'=>[
                ['name'=>'Qovun','date'=>3],
                ['name'=>'Tarvuz','date'=>4],
                ['name'=>'Osh Qovoq','date'=>2],
                ['name'=>'Bodiring','date'=>2],
                ['name'=>'Pomildori','date'=>2],
            ],
            'Tozalash vositalari'=>[
                ['name'=>'Crustal','date'=>13],
                ['name'=>'Olmos','date'=>11],
                ['name'=>'Kir Sovun','date'=>12],
                ['name'=>'Niveya','date'=>14],
                ['name'=>'Clear','date'=>15],
            ]

        ];

 /* Egamberdi Domla, Ctegory  Bilan ,Products  Bitta Seederga yozdim birini Id sini Ikkinchisiga
                    Yozish Oson Bo'lishi Uchun  */

      foreach($datas as $indexData=>$data){
      $c_id =  category::create(['name'=>$indexData]);
      foreach($data as $da){
          product::create(['name'=>$da['name'],'category_id'=>$c_id->id, 'keep_date'=>$da['date']]);
      }
      }

       
    }
}
